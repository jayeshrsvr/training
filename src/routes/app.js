import React from 'react';
import {
  BrowserRouter, Link, Route, Switch
} from "react-router-dom";
import About from '../pages/About';
import AboutName from '../pages/AboutName';
import FormikForm from '../pages/formikform';
import Home from '../pages/Home';
import Profile from '../pages/Profile';
import PublicProfile from '../pages/ProfileUser';

const AppRoutes =() => {
    return (
        <BrowserRouter>
        <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/profile">Users</Link>
            </li>
            <li>
              <Link to="/Formik">Formik forms</Link>
            </li>
          </ul>
        </nav>
</div>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact  path="/about" component={About} />
                <Route exact  path="/Formik" component={FormikForm} />
                <Route exact path="/profile" component={Profile} />
                 <Route  path="/profile/@:value" component={PublicProfile} />
                <Route path="/about/:name" component={AboutName} />
            </Switch>
        </BrowserRouter>
    )
}
export default  AppRoutes


   