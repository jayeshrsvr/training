import React from 'react'
import ComA from '../components/ComA'
import ComB from '../components/ComB'
import ComC from '../components/ComC'
class Page1 extends React.Component {
    render() {
 return(
    <div>
        <h1>
            Component {this.props.name}
        </h1>
        <ComA/>
        <ComB/>
        <ComC/>
    </div> 
 ) 
}
}

export default Page1