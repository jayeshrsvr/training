import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import '../App.css';
import { decNum, incNum } from '../redux/actions/index';
const Home =()=> {
   const mystate = useSelector((state) => state.change);
   const dispatch = useDispatch();
 return(
   <div>
   <h2>Increment/Decrement the number using Redux.</h2>
   <div className="App">
     <button  onClick={() => dispatch(incNum())}>+</button>
     <h1>{mystate}</h1>
     <button onClick={() => dispatch(decNum())}>-</button>
   </div>
 </div>
 ) 

}

export default Home