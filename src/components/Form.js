import React from 'react'

const Form=()=> {
 const formRef =React.useRef(null)
    const [state, setState] = React.useState({
        		username: '',
        	}
        )

	const handleUsernameChange = (event) => {
		setState({
			username: event.target.value
		})
	}



	const handleSubmit = (event) => {
		alert(`${state.username}`)
		event.preventDefault()
	}

	const onButtonClick=()=>{
		formRef.current.focus()
		// formRef.current.clear()
	}


		return (
			<form onSubmit={handleSubmit}>
                {/* <h1>
                    class component
                    </h1> */}
				<div>
					<label>name </label>
					<input
					ref={formRef}
						type="text"
						value={state.username}
						onChange={handleUsernameChange}
					/>
				</div>
				<button type='button' onClick={onButtonClick}>Edit name</button>
				<button type="submit">Save</button>
			</form>
		)
	
}

export default Form