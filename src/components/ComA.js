import React from 'react';
import Hoc from './Hoc';
class ComA extends React.Component {
    render() {
    
 return(
    <div>
        <h1>
            Component  A
        </h1>
        {/* <ComB nameB={this.props.info}/> */}
    </div> 
 ) 
}
}
const EnhancedComA =Hoc(ComA,'from a class component A')

export default EnhancedComA;