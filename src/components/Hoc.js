import React from "react";
const Hoc=(Component,data)=>{

    return(
        function(){
            return(
                <div>
                    <p>Comming from HOC</p>
                    data:{data}
                    <Component/>
                </div>
            )
        }
    )
}

export default Hoc