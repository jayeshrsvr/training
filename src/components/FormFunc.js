import React from 'react';

const FunctionalForm =()=> {
	//initial state
	let [info, setInfo] = React.useState({
		username: '',//coder
		password: '',
		description: ''
	  });
	
   //handle change  function
	  let handleChange = (e) => {
		let name = e.target.name;//username
		let value = e.target.value;//coder
		info[name] = value;  //info[username]= coder
		setInfo(info);
	  }
	//submit function 
	  let save = (e) => {
		e.preventDefault();
		console.log(info);
	  }
	  //validation 
	
	  return (
		<div>
			<legend>Demo 2</legend>
			<form method="post" onSubmit={save}>
			  Username <input type="text" name="username" onChange={handleChange} value={state.username} />
			  <br />
			  Password <input type="password" name="password" onChange={handleChange} />
			  <br />
			  Bio
			  <br />
			  <textarea name="description" cols="20" rows="5" onChange={handleChange}></textarea>
			  <br />
			  <input type="submit" value="Save" />
			</form>
	
		</div>
	  );
	};

export default FunctionalForm